from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.settings import settings
from app.core import get_pool
from app.api.handlers.author_handlers import router as author_router
from app.api.handlers.history_message_handlers import router as hs_router
from app.api.handlers.srv import router as srv_router

app = FastAPI(
    docs_url="/sm_docs",
    title="smolathon",
    description="",
    openapi_url="/openapi.json",
)

origins = [
    "http://localhost.tiangolo.com",
    "https://localhost.tiangolo.com",
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(author_router)
app.include_router(hs_router)
app.include_router(srv_router)

@app.on_event("startup")
async def startup_event():
    pool = await get_pool(settings.pg_dsn)
    settings.pool = pool

@app.get("/health")
async def read_root():
    async with settings.pool.acquire() as conn:
        res = await conn.fetch("SELECT 1")
        return res
