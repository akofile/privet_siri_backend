
# ХисториХаб

Бэкэнд модуль проекта, разработанный с помощью python + fastapi

## Запуск
Рекомендуется создать виртуальное окружение
```bash
pip install -r requirements.txt
```
```bash
uvicorn main:app --reload --host 0.0.0.0
```
Или можно воспользоваться докером
документация openapi доступна по адресу http://localhost:8000/sm_docs#/
(хост порт по вкусу)

## Зависимости
postgresql - ссылку для подключения в переменную окружения 
pg_dsn

Применить миграции с установленноей библиотекой yoyo
Из папки db
```bash
yoyo apply -d postgres://postgres:pass@localhost:5432/db -b ./migrations
```
Можно заполнить тестовыми данными через app/api/handlers/_mock_data
или воспользоваться srv ручкой
