from asyncpg import create_pool


async def get_pool(url):
    return await create_pool(dsn=url)
