from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field


class AuthorOutputSchema(BaseModel):
    author_id: int = Field(..., description="id")
    type: str = Field(...)
    name: str = Field(...)
    address: str = Field(...)
    latitude: float = Field(...)
    longitude: float = Field(...)
    created_dt: datetime = Field(...)
    updated_dt: Optional[datetime] = Field(None)
    archived_dt: Optional[datetime] = Field(None)
