import ast
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field, field_validator, root_validator, model_validator


class ImageUrlOutputSchema(BaseModel):
    url: str = Field(..., description="Ссылка на фото")
    description: str = Field(..., description="Подпись под фото")


class HistoryMessageOutputSchema(BaseModel):
    history_message_id: int = Field(..., description="id")
    description: str = Field(..., description="Описание события")
    name: str = Field(...)
    latitude: float = Field(...)
    longitude: float = Field(...)
    type: Optional[str] = Field(None)
    author_id: int = Field(...)
    current_loaction: str = Field(...)
    images_url: list[ImageUrlOutputSchema] = Field(...)
    history_dt: datetime = Field(...)
    created_dt: datetime = Field(...)

    @model_validator(mode="before")
    def parse_a_obj(cls, values):
        values.images_url = [ast.literal_eval(v) for v in values.images_url]
        return values
