from app.models.history_message_model import HistoryMessageModel
from app.settings import settings


class HistoryMessageService:
    @staticmethod
    async def get_author_by_type(author_id: int):
        async with settings.pool.acquire() as conn:
            return await HistoryMessageModel.get_messages_by_filters(
                conn, {"author_id": author_id}
            )
