from app.models.author_model import AuthorModel
from app.settings import settings


class AuthorService:
    @staticmethod
    async def get_author_by_type(type: str):
        async with settings.pool.acquire() as conn:
            return await AuthorModel.get_author_by_type(conn, {"type": type})
