from fastapi import APIRouter

from app.settings import settings

router = APIRouter(prefix="/srv", tags=["srv"])


@router.get(
    "/data",
    description="Получить музеи/частных лиц",
)
async def generate_data():
    async with settings.pool.acquire() as conn:
        with open("app/api/handlers/_mock_data/authors.sql") as f:
            await conn.execute(f.read())
        with open("app/api/handlers/_mock_data/history_messages.sql.sql") as f:
            await conn.execute(f.read())
    return "ok"
