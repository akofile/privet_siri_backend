from fastapi import APIRouter

from app.api.schemas.author_schemas import AuthorOutputSchema
from app.api.services.author_service import AuthorService

router = APIRouter(prefix="/author", tags=["authors"])


@router.get(
    "/{type}",
    response_model=list[AuthorOutputSchema],
    description="Получить музеи/частных лиц",
)
async def get_author(type: str):
    res = await AuthorService.get_author_by_type(type)
    return res
