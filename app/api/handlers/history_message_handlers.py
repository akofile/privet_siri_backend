from fastapi import APIRouter

from app.api.schemas.history_message_schemas import HistoryMessageOutputSchema
from app.api.services.history_message_service import HistoryMessageService

router = APIRouter(prefix="/history-message", tags=["hs"])


@router.get("/{author_id}", response_model=list[HistoryMessageOutputSchema])
async def get_history_message(author_id: int):
    res = await HistoryMessageService.get_author_by_type(author_id)
    return res
