from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class AuthorModel:
    author_id: int
    type: str
    name: str
    address: str
    latitude: Optional[float]
    longitude: Optional[float]
    created_dt: datetime

    @classmethod
    async def get_author_by_type(cls, conn, filters) -> "AuthorMessage":
        array_filters = []
        if filters.get("type"):
            array_filters.append(f"type = '{filters.get('type')}'")
        filters_for_query = " AND ".join(array_filters)
        get_messages_query = f"""
            SELECT 
                author_id,
                type,
                name,
                address,
                latitude,
                longitude,
                created_dt
            FROM author WHERE archived_dt is null and {filters_for_query}
        """
        result = await conn.fetch(get_messages_query)
        return [cls(**row) for row in result]
