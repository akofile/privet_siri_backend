from dataclasses import dataclass
from datetime import datetime

from app.settings import settings


@dataclass
class ImageModel:
    history_message_id: int
    description: str
    name: str
    latitude: float
    longitude: float
    type: str
    author_id: int
    current_loaction: str
    history_dt: datetime
    created_dt: datetime

    @classmethod
    async def get_messages_by_filters(cls, conn, filters):
        array_filters = []
        filters_for_query = " AND ".join(array_filters)
        get_messages_query = f"""
            SELECT history_message_id,
            description,
            name,
            latitude,
            longitude,
            type,
            author_id,
            current_loaction,
            history_dt,
            created_dt
            FROM history_message
            WHERE archieved_dt is not null and ({filters_for_query})
        """
        result = conn.fetch(get_messages_query)
        return [cls(**row) for row in result]
