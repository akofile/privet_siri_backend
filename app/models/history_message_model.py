from dataclasses import dataclass
from datetime import datetime

from app.settings import settings


@dataclass
class HistoryMessageModel:
    history_message_id: int
    description: str
    name: str
    latitude: float
    longitude: float
    type: str
    author_id: int
    current_loaction: str
    images_url: dict
    history_dt: datetime
    created_dt: datetime

    @classmethod
    async def get_messages_by_filters(cls, conn, filters):
        array_filters = []
        if filters.get("author_id"):
            array_filters.append(f"author_id = '{filters.get('author_id')}'")
        filters_for_query = " AND ".join(array_filters)
        get_messages_query = f"""
            SELECT history_message_id,
            description,
            name,
            latitude,
            longitude,
            type,
            author_id,
            current_loaction,
            images_url,
            history_dt,
            created_dt
            FROM history_message
            WHERE archived_dt is null and ({filters_for_query}) ORDER BY history_dt
        """
        result = await conn.fetch(get_messages_query)
        return [cls(**row) for row in result]
