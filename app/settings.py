from typing import Optional

from pydantic import (
    PostgresDsn,
)

from pydantic_settings import BaseSettings
from asyncpg import Pool


class Settings(BaseSettings):
    pg_dsn: str = "postgres://postgres:pass@localhost:5432/db"
    pool: Optional[Pool] = None


settings = Settings()
