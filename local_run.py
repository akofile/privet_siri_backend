from uvicorn import run
from main import app

run(app, port=8001, host="0.0.0.0")